const GameEngine = require("../lib/game_engine");

describe("The Game Engine must", () => {
  test("start a new game with a random word", () => {
    var gamestate = GameEngine.startGame();
    expect(gamestate.lives).toBe(5);
    expect(gamestate.guesses.length).toBe(0);
    expect(gamestate.word.length).toBeGreaterThan(3);
    expect(gamestate.display_word.length).toBe(2*gamestate.word.length-1);

  });

  test("update the guesses and lives when a wrong guess is given", () => {
    var gamestate = GameEngine.startGame();
    gamestate.word="fujitsu";
    let new_game_state= GameEngine.takeGuess(gamestate,"a");
    expect(new_game_state.lives).toBe(4);
    expect(new_game_state.guesses.length).toBe(1);
  });

  test("update the guesses and lives when you lose the game", () => {
    var gamestate = GameEngine.startGame();
    gamestate.word="fujitsu";
    gamestate.lives = 1

  

    let new_game_state= GameEngine.takeGuess(gamestate,"a");
    expect(new_game_state.lives).toBe(0);
    expect(new_game_state.status).toBe("GameOver");
  
  });
});
