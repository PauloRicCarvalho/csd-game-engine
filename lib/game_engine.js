const random_line = require("@carvalhopa/random-words");



const startGame = () => {
  let word = random_line.getRandomWord()
  return {
    status: "RUNNING",
    word: word,
    lives: 5,
    display_word: create_display_word(word,[]),
    guesses: []
  };
};

const takeGuess = (game_state, guess) => {
  let guess_is_right=game_state.word.indexOf(guess)>=0;

  if(!guess_is_right)
  {
    game_state.lives--;
    game_state.guesses.push(guess);
  }

  if(game_state.lives==0)
  {
    game_state.status="GameOver";
  }

  if(game_state.status=="GameOver")
{
  console.log("Game Over");
}
  return game_state;
};

const add_spaces = word => {
  return word
  .split("")
  .join(" ")
  .toUpperCase();
};

const create_display_word = (word, guesses) => {
  let display_word = "";

  for (let letter of word) {
    if (guesses.indexOf(letter)>= 0) display_word += letter;
    else display_word += "_";

  };
  return add_spaces(display_word);
};



module.exports = {
  startGame,
  takeGuess
};
